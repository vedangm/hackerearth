from django.db import models

class SystemUsage(models.Model):
	"""Contains records of timestamp, cpu and memory for the system_monitor app"""
	timestamp = models.DateTimeField('date and time')
	cpu = models.FloatField()
	memory = models.FloatField()