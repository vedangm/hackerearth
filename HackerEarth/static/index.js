console.clear();
$(document).ready(function() {
  $("#mainlink").click(function() {
    var isVisible = $('#submenu').is(":visible");
    if (!isVisible) {
      $('.js-add').hide();
      $('.js-remove').show();
    } else {
      $('.js-remove').hide();
      $('.js-add').show();
    }
    $('#submenu').toggle('show');

  });
});