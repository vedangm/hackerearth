import random
import time
from django.shortcuts import render, render_to_response
from django.http import JsonResponse


# Create your views here.

def generate_number(request):
    data = {
        'y': random.randint(1, 10),
        'x': time.time() * 1000
    }
    return JsonResponse(data)


def index(request):
    return render(request, './random_integers.html')
