from django.contrib import admin
from django.urls import include, path


urlpatterns = [
    path('', include('random_integers.urls')),
    path('stocks/', include('stocks.urls')),
    path('system_info/', include('system_monitor.urls')),
    #path('', include('stocks.urls')),
    path('admin/', admin.site.urls)
]
