from django.apps import AppConfig


class RandomIntegersConfig(AppConfig):
    name = 'random_integers'
