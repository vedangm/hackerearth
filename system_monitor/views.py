import time
import datetime
import psutil
from django.shortcuts import render, render_to_response
from django.http import JsonResponse
from django.utils import timezone
from .models import SystemUsage


def index(request):
    return render(request, './system_monitor.html')


def get_system_info(request):
    cpu = psutil.cpu_percent()
    memory = psutil.virtual_memory().percent
    current_time = time.time() * 1000
    data = {'cpu': cpu, 'memory': memory, 'current_time': current_time}
    usage = SystemUsage()
    usage.cpu = cpu
    usage.memory = memory
    # usage.timestamp = datetime.datetime.fromtimestamp(current_time / 1000).strftime('%Y-%m-%d %H:%M:%S')
    usage.timestamp = datetime.datetime.fromtimestamp(
        current_time / 1000, tz=timezone.utc)
    usage.save()
    return JsonResponse(data)
