import random
import re
import time
import datetime
import json
import ast
import requests
from django.shortcuts import render, render_to_response
from django.http import JsonResponse


def index(request):
	response = render(request, './index.html')
	response.set_cookie(key = "first_load", value = "true")
	return response


def get_stock_data(request):
	"""Returns stock data for Apple, Intuit and Facebook"""
	stock_codes = ["AAPL", "INTU", "FB"]
	stock_data = {}
	for code in stock_codes:
		url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={}&interval=1min&outputsize=full&apikey=HOY25QHT2NGBX2S7'.format(
            code)
		r = requests.get(url)
		data = r.text
		d = ast.literal_eval(data)
		stock_quotes = [(k, float(v['4. close']))
                        for k, v in d['Time Series (1min)'].items()]
		stock_quotes = sorted([[time.mktime(datetime.datetime.strptime(
            quote[0], "%Y-%m-%d %H:%M:%S").timetuple()) * 1000, quote[1]] for quote in stock_quotes])
		# Sending all data for the first load, but only the new data point for the consequent loads
		if request.COOKIES["first_load"] == "true":
			stock_data[code] = stock_quotes
		else:
			stock_data[code] = stock_quotes[:-1][-1]
	response = JsonResponse(stock_data)
	response.set_cookie(key = "first_load", value = "false")
	return response