from django.contrib import admin

from .models import SystemUsage

admin.site.register(SystemUsage)
