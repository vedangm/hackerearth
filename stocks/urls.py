from django.conf.urls import url
from . import views

urlpatterns = [
    # url(r'^/$', views.index, name='index'),  # index view at /
    url(r'^$', views.index),   # likepost view at /likepost
    # likepost view at /likepost
    url(r'^get_stock_data$', views.get_stock_data),
]
